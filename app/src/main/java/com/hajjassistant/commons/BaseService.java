package com.hajjassistant.commons;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.hajjassistant.app.HajjApp;
import com.hajjassistant.app.di.AppComponent;

public class BaseService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected AppComponent getAppComponent() {
        return ((HajjApp) getApplication()).getAppComponent();
    }
}
