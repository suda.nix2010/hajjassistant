package com.hajjassistant.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class PreferenceUtil {

    private Context mContext;

    public PreferenceUtil(Context context) {
        this.mContext = context;
    }

    public void save(String key, Object object) {
        String val = new Gson().toJson(object);
        setStringValue(key, val);
    }

    public <T> Object get(String key, Class<T> clazz) {
        String value = getStringValue(key);
        return new Gson().fromJson(value, clazz);
    }

    public <T> Object get(String key, Type type) {
        String value = getStringValue(key);
        return new Gson().fromJson(value, type);
    }

    public void removeValue(String key) {
        SharedPreferences.Editor editor = getDefaultEditor();
        editor.remove(key);
        editor.commit();
    }

    public String getStringValue(String key) {
        SharedPreferences sharedPreferences = getDefaultSharedPreferences();
        return sharedPreferences.getString(key, "");
    }

    public String getStringValue(String key, String defaultValue) {
        SharedPreferences sharedPreferences = getDefaultSharedPreferences();
        return sharedPreferences.getString(key, defaultValue);
    }

    public Boolean getBooleanValue(String key, boolean defaultValue) {
        SharedPreferences sharedPreferences = getDefaultSharedPreferences();
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public int getIntValue(String key, int defaultValue) {
        SharedPreferences sharedPreferences = getDefaultSharedPreferences();
        int value = sharedPreferences.getInt(key, defaultValue);
        return value;
    }

    public void setStringValue(String key, String value) {
        SharedPreferences.Editor editor = getDefaultEditor();
        editor.putString(key, value);
        editor.commit();
    }

    public void setIntValue(String key, int value) {
        SharedPreferences.Editor editor = getDefaultEditor();
        editor.putInt(key, value);
        editor.commit();
    }

    public void setBooleanValue(String key, boolean value) {
        SharedPreferences.Editor editor = getDefaultEditor();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private SharedPreferences.Editor getDefaultEditor() {
        return getDefaultSharedPreferences().edit();
    }

    private SharedPreferences getDefaultSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void clear() {
        getDefaultEditor().clear().commit();
    }

    public void clear(String userKey) {
        getDefaultEditor().remove(userKey).commit();
    }
}
