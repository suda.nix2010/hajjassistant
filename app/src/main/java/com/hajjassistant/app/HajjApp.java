package com.hajjassistant.app;

import android.annotation.TargetApi;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;

import com.hajjassistant.R;
import com.hajjassistant.app.di.AppComponent;
import com.hajjassistant.app.di.AppModule;
import com.hajjassistant.app.di.DaggerAppComponent;
import com.hajjassistant.app.di.RetrofitModule;
import com.hajjassistant.utils.LocaleUtil;
import com.karumi.dexter.Dexter;

import javax.inject.Inject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class HajjApp extends Application {

    private static final String TAG = "HajjApp";

    private AppComponent mAppComponent;

    @Inject
    LocaleUtil mLocaleUtil;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule())
                .build();

        mAppComponent.inject(this);

        mLocaleUtil.initAppLocale();

        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.app_font))
                        .setFontAttrId(R.attr.fontPath)
                        .addCustomStyle(AppCompatTextView.class, android.R.attr.textViewStyle)
                        .build());

        Dexter.initialize(this);

        createNotificationChannel();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public void setAppComponent(AppComponent appComponent) {
        mAppComponent = appComponent;
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            CharSequence channelName = "Order statuses";
            String channelId = "order_status_changes";

            if (notificationManager.getNotificationChannel(channelId) == null) {
                Log.v(TAG, ">>>>>>>>> createNotificationChannel: " + channelId);
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.GREEN);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }
}
