package com.hajjassistant.app.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hajjassistant.app.ApiEndpoint;
import com.hajjassistant.app.Session;
import com.hajjassistant.commons.AuthHeaderInterceptor;
import com.hajjassistant.domain.ApiErrorResponse;
import com.hajjassistant.utils.LocaleUtil;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class RetrofitModule {

    @Provides
    @Singleton
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    public AuthHeaderInterceptor provideAuthHeaderInterceptor(Session session) {
        return new AuthHeaderInterceptor(session);
    }

    @Provides
    @Singleton
    public Interceptor provideInterceptor(final LocaleUtil localeUtil) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("lang", localeUtil.getLocale()).build();
                return chain.proceed(request);
            }
        };
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor,
                                            Interceptor languageInterceptor,
                                            AuthHeaderInterceptor authHeaderInterceptor) {
        return new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(languageInterceptor)
                .addInterceptor(authHeaderInterceptor)
                .build();
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder()
                .create();
    }

    @Provides
    @Singleton
    public Converter<ResponseBody, ApiErrorResponse> provideErrorResponseBodyConverter(Retrofit retrofit) {
        return retrofit.responseBodyConverter(ApiErrorResponse.class, new Annotation[0]);
    }

    @Provides
    @Singleton
    public Retrofit provideRxJavaRetrofit(OkHttpClient okHttpClient,
                                          ApiEndpoint apiEndpoint,
                                          Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(apiEndpoint.getApiUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }
}
