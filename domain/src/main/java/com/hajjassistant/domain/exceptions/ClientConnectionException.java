package com.hajjassistant.domain.exceptions;

public class ClientConnectionException extends ConnectionException {

    public ClientConnectionException(String message) {
        super(message);
    }
}
