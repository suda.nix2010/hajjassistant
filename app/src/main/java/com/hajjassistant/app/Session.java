package com.hajjassistant.app;


import com.hajjassistant.domain.user.User;
import com.hajjassistant.utils.LocaleUtil;
import com.hajjassistant.utils.PreferenceUtil;

public class Session {

    private static final String USER_KEY = "USER_KEY";
    private static final String ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY";

    private static final String DEVICE_GCM_TOKEN_KEY = "DEVICE_GCM_TOKEN_KEY";

    private PreferenceUtil mPreferenceUtil;
    private LocaleUtil mLocaleUtil;

    // Cached objects
    private static User mUser;
    private static String mAccessToken;

    public Session(PreferenceUtil preferenceUtil, LocaleUtil localeUtil) {
        mPreferenceUtil = preferenceUtil;
        mLocaleUtil = localeUtil;
    }

    public User getUser() {
        if (mUser == null) {
            mUser = (User) mPreferenceUtil.get(USER_KEY, User.class);
        }

        return mUser;
    }

    public String getAccessToken() {
        if (mAccessToken == null) {
            mAccessToken = (String) mPreferenceUtil.get(ACCESS_TOKEN_KEY, String.class);
        }

        return mAccessToken;
    }

    public void logout() {
        mUser = null;
        mAccessToken = null;

        mPreferenceUtil.clear(USER_KEY);
        mPreferenceUtil.clear(ACCESS_TOKEN_KEY);
        mPreferenceUtil.clear(DEVICE_GCM_TOKEN_KEY);
    }

    public boolean isArabicLocale() {
        return mLocaleUtil.isArabicLocale();
    }

    public String getCurrentLanguage() {
        if (isArabicLocale()) {
            return "ARABIC";
        } else {
            return "ENGLISH";
        }
    }

    public void saveUser(final User user) {
        // Cache date
        mUser = user;

        new Thread(new Runnable() {
            @Override
            public void run() {
                mPreferenceUtil.save(USER_KEY, user);
            }
        }).start();
    }

    public void saveAccessToken(final String accessToken) {
        // Cache date
        mAccessToken = accessToken;

        new Thread(new Runnable() {
            @Override
            public void run() {
                mPreferenceUtil.save(ACCESS_TOKEN_KEY, accessToken);
            }
        }).start();
    }

    /*
    ** Called only from Firebase service to update current token
     */
    public void setDeviceGcmToken(String gcmToken) {
        mPreferenceUtil.setStringValue(DEVICE_GCM_TOKEN_KEY, gcmToken);
    }

    public String getDeviceGcmToken() {
        return mPreferenceUtil.getStringValue(DEVICE_GCM_TOKEN_KEY);
    }

    public boolean isLoggedIn() {
        return getUser() != null;
    }
}
