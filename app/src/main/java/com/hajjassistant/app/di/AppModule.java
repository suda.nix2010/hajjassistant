package com.hajjassistant.app.di;

import android.content.Context;
import android.content.res.AssetManager;

import com.hajjassistant.app.ApiEndpoint;
import com.hajjassistant.app.HajjApp;
import com.hajjassistant.app.Session;
import com.hajjassistant.utils.LocaleUtil;
import com.hajjassistant.utils.MobileValidator;
import com.hajjassistant.utils.PreferenceUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private HajjApp mApp;

    public AppModule(HajjApp mohApp) {
        mApp = mohApp;
    }

    @Provides
    @Singleton
    public MobileValidator provideMobileValidator() {
        return new MobileValidator();
    }

    @Provides
    @Singleton
    public ApiEndpoint provideApiEndpoint(Context context) {
        return new ApiEndpoint(context);
    }

    @Provides
    @Singleton
    public PreferenceUtil providePreferenceUtil(HajjApp app) {
        return new PreferenceUtil(app);
    }

    @Provides
    @Singleton
    public LocaleUtil provideLocaleUtil(HajjApp app) {
        return new LocaleUtil(app);
    }

    @Provides
    @Singleton
    public Session provideSession(PreferenceUtil preferenceUtil, LocaleUtil localeUtil) {
        return new Session(preferenceUtil, localeUtil);
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return mApp;
    }

    @Provides
    @Singleton
    public AssetManager provideAssetManager() {
        return mApp.getApplicationContext().getAssets();
    }

    @Provides
    @Singleton
    public HajjApp provideLemonApp() {
        return mApp;
    }
}
