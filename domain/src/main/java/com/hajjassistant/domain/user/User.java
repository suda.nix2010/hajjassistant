package com.hajjassistant.domain.user;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("user_id")
    String userId;

    @SerializedName("full_name")
    String fullName;

    @SerializedName("thumbnail_url")
    String thumbnailUrl;

    @SerializedName("mobile")
    String mobileNumber;

    @SerializedName("mobile_activated")
    Boolean mobileActivated;

    @SerializedName("email")
    String email;

    public String getUserId() {
        return userId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public Boolean getMobileActivated() {
        return mobileActivated;
    }
}
