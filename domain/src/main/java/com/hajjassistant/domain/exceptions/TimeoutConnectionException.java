package com.hajjassistant.domain.exceptions;

public class TimeoutConnectionException extends ConnectionException {

    public TimeoutConnectionException(String message) {
        super(message);
    }
}
