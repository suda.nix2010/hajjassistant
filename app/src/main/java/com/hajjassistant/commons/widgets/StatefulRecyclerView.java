package com.hajjassistant.commons.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.hajjassistant.R;

public class StatefulRecyclerView extends StatefulGroupView
        implements StatefulGroupView.EmptyContentCallback {

    private RecyclerView mContentView;

    public StatefulRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerView getRecyclerView() {
        return mContentView;
    }

    public void init(RecyclerView.Adapter adapter,
                     StatefulGroupView.TryAgainClickListener tryAgainClickListener) {
        super.init(tryAgainClickListener, this);
        mContentView = (RecyclerView) getContentView();

        mContentView.setAdapter(adapter);
    }

    @Override
    public boolean isEmptyContent() {
        return mContentView.getAdapter().getItemCount() == 0;
    }

    protected int getLayoutId() {
        return R.layout.view_stateful_recyclerview;
    }

}
