package com.hajjassistant.commons;

import android.text.TextUtils;

import com.hajjassistant.app.Session;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthHeaderInterceptor implements Interceptor {

    private Session session;

    public AuthHeaderInterceptor(Session session) {
        this.session = session;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = session.getAccessToken();
        Request request = chain.request();

        if (request.header("No-Authentication") == null && !TextUtils.isEmpty(token)) {
            request = request.newBuilder().addHeader("Authorization", "Bearer " + token).build();
        } else {
            request = request.newBuilder().addHeader("Authorization", "Basic TEVNT05fVEFYSV9TVVBQT1JUX0lEOjQ0NDRjOTQ4NzJiZmVhNGY5ZjJjYWZkMGEwMTRkZTU5ZDBjMzAzYTIyMjIzOGUyNTU0NDEzMDM1ZjJlNjA5MjE=").build();
        }

        return chain.proceed(request);
    }
}
