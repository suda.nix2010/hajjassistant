package com.hajjassistant

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.hajjassistant.items.*
import mehdi.sakout.fancybuttons.FancyButton

open class VerticalRecyclerAdapter(private val items: ArrayList<ArrayList<Item>>?,
                                   private val itemClickListener: ItemClickListener) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val listPosition = SparseIntArray()

    private var listListItemClickListener: HorizontalRecyclerAdapter.OnListItemClickListener? = null
    private var mContext: Context? = null

    enum class ItemTypes(val value: Int) {
        ITEM(1),
        CHAT_SEND_ITEM(2),
        CHAT_RECEIVE_ITEM(3),
        MAP_ITEM(4),
        INFO_ITEM(5),
        EXTERNAL_LINK_ITEM(6),
        HORIZONTAL_LIST_ITEM(7),
        BANNER_ITEM(8),
        INFO2_ITEM(9),
        MOBILE_ITEM(10),
        VIDEO_ITEM(11)
    }

    override fun getItemViewType(position: Int): Int {
        val list = items?.get(position)

        if (list != null && list.size > 1) {
            return ItemTypes.HORIZONTAL_LIST_ITEM.value
        }

        val item = list?.first()

        if (item is InfoItem || item is Info2Item || item is ImageItem || item is MobileItem || item is VideoItem) {
            return ItemTypes.HORIZONTAL_LIST_ITEM.value
        }

        return when (item) {
            is ChatSendItem -> ItemTypes.CHAT_SEND_ITEM.value
            is ChatReceiveItem -> ItemTypes.CHAT_RECEIVE_ITEM.value
            is MapItem -> ItemTypes.MAP_ITEM.value
            is InfoItem -> ItemTypes.INFO_ITEM.value
            is Info2Item -> ItemTypes.INFO2_ITEM.value
            is ExternalLinkItem -> ItemTypes.EXTERNAL_LINK_ITEM.value
            is BannerItem -> ItemTypes.BANNER_ITEM.value
            is MobileItem -> ItemTypes.MOBILE_ITEM.value
            is VideoItem -> ItemTypes.VIDEO_ITEM.value
            else -> ItemTypes.ITEM.value
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = viewGroup.context
        val inflater = LayoutInflater.from(viewGroup.context)

        return when (viewType) {
            ItemTypes.HORIZONTAL_LIST_ITEM.value -> {
                HorizontalListViewHolder(inflater.inflate(R.layout.item_list, viewGroup, false))
            }
            ItemTypes.CHAT_SEND_ITEM.value -> {
                ChatSendViewHolder(inflater.inflate(R.layout.item_message_send, viewGroup, false))
            }
            ItemTypes.CHAT_RECEIVE_ITEM.value -> {
                ChatReceiveViewHolder(inflater.inflate(R.layout.item_message_received, viewGroup, false))
            }
            ItemTypes.BANNER_ITEM.value -> {
                BannerItemViewHolder(inflater.inflate(R.layout.banner_item_view, viewGroup, false))
            }
            else -> {
                ChatSendViewHolder(inflater.inflate(R.layout.item_message_send, viewGroup, false))
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        when (viewHolder) {
            is HorizontalListViewHolder -> {
                viewHolder.recyclerView.setHasFixedSize(true)
                val layoutManager = LinearLayoutManager(mContext)
                layoutManager.orientation = LinearLayoutManager.HORIZONTAL
                viewHolder.recyclerView.layoutManager = layoutManager
                val arrayListItems = items!![position]
                val adapter = HorizontalRecyclerAdapter(arrayListItems)
                viewHolder.recyclerView.adapter = adapter

                viewHolder.sectionTitleTextView.visibility = View.GONE
                viewHolder.sectionSubTitleTextView.visibility = View.INVISIBLE
                viewHolder.actionButton.visibility = View.GONE

                if (arrayListItems.isNotEmpty()) {
                    val item = arrayListItems.first()
                    if ((item is InfoItem) && item.metaData != null) {
                        viewHolder.sectionTitleTextView.text = item.metaData.title
                        viewHolder.sectionSubTitleTextView.text = item.metaData.subtitle
                        viewHolder.actionButton.setText(item.metaData.actionButtonName)
                        viewHolder.actionButton.setBackgroundColor(Color.parseColor(item.metaData.actionButtonColor))

                        viewHolder.sectionTitleTextView.visibility = View.VISIBLE
                        viewHolder.sectionSubTitleTextView.visibility = View.VISIBLE
                        viewHolder.actionButton.visibility = View.VISIBLE

                        viewHolder.actionButton.setOnClickListener {
                            listListItemClickListener?.onActionButtonClicked(item)
                        }
                    }
                }

                adapter.setOnItemClickListener(listListItemClickListener)

                val lastSeenFirstPosition = listPosition.get(position, 0)
                if (lastSeenFirstPosition >= 0) {
                    viewHolder.recyclerView.scrollToPosition(lastSeenFirstPosition)
                }
            }

            is ChatSendViewHolder -> {
                val list = items?.get(position)
                val chatItem = list?.first()
                viewHolder.messageSentTextView.text = chatItem?.title
            }

            is ChatReceiveViewHolder -> {
                val list = items?.get(position)
                val chatItem = list?.first()
                viewHolder.messageReceivedTextView.text = chatItem?.title
            }

            is BannerItemViewHolder -> {
                val list = items?.get(position)
                val bannerItem = list?.first()
                val item = bannerItem as BannerItem
                viewHolder.title.text = item.title
                viewHolder.date.text = item.date
                viewHolder.subtitle.text = item.desc

                viewHolder.viewUpdateButton.setOnClickListener {
                    itemClickListener.onItemClicked(bannerItem)
                }
            }
        }
    }

    override fun onViewRecycled(viewHolder: RecyclerView.ViewHolder) {
        if (viewHolder is HorizontalListViewHolder) {
            val position = viewHolder.adapterPosition
            val cellViewHolder = viewHolder as HorizontalListViewHolder
            val layoutManager = cellViewHolder.recyclerView.layoutManager as LinearLayoutManager
            val firstVisiblePosition = layoutManager.findFirstVisibleItemPosition()
            listPosition.put(position, firstVisiblePosition)
        }

        super.onViewRecycled(viewHolder)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    fun add(item: Item) {
        val element = ArrayList<Item>()
        element.add(item)
        items?.add(element)
        notifyDataSetChanged()
    }

    fun addList(element: ArrayList<Item>) {
        items?.add(element)
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(listItemClickListener: HorizontalRecyclerAdapter.OnListItemClickListener) {
        this.listListItemClickListener = listItemClickListener
    }

    fun addItemLists(list: ArrayList<ArrayList<Item>>) {
        items?.addAll(list)
        notifyDataSetChanged()
    }

    class HorizontalListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val recyclerView: RecyclerView = itemView.findViewById<View>(R.id.recyclerView) as RecyclerView
        val sectionTitleTextView: TextView = itemView.findViewById<View>(R.id.sectionTitleTextView) as TextView
        val sectionSubTitleTextView: TextView = itemView.findViewById<View>(R.id.sectionSubTitleTextView) as TextView
        var actionButton: FancyButton = itemView.findViewById<View>(R.id.actionButton) as FancyButton
    }

    class ChatSendViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val messageSentTextView: TextView = itemView.findViewById<View>(R.id.messageSentTextView) as TextView
    }

    class ChatReceiveViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val messageReceivedTextView: TextView = itemView.findViewById<View>(R.id.messageReceivedTextView) as TextView
    }

    class VideoItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView = itemView
        val textView: TextView = itemView.findViewById<View>(R.id.text) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.imageView) as ImageView
    }

    class MobileItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView = itemView
    }

    class BannerItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView = itemView
        val title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        val date: TextView = itemView.findViewById<View>(R.id.date) as TextView
        val subtitle: TextView = itemView.findViewById<View>(R.id.desc) as TextView
        val viewUpdateButton: FancyButton = itemView.findViewById<View>(R.id.viewUpdateButton) as FancyButton
    }

    interface ItemClickListener {
        fun onItemClicked(item: Item)
    }
}