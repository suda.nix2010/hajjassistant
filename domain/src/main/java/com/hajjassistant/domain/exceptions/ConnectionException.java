package com.hajjassistant.domain.exceptions;

public class ConnectionException extends Exception {

    public ConnectionException(String message) {
        super(message);
    }
}
