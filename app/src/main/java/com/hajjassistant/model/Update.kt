package com.hajjassistant.model


class Update {
    var title: String? = null
    var subtitle: String? = null
    var date: String? = null
}