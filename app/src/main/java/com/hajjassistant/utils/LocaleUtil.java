package com.hajjassistant.utils;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class LocaleUtil {

    public static final String APP_LANG_KEY_EN = Language.ENGLISH.toString();
    public static final String APP_LANG_KEY_AR = Language.ARABIC.toString();

    private static final String APP_LANG_KEY = "app_lang";

    private Context mContext;

    public LocaleUtil(Context context) {
        this.mContext = context;
    }

    public void swapLocale() {
        savePreferenceAppLanguage(isArabicLocale() ? Language.ENGLISH.toString() :
                Language.ARABIC.toString());

        initAppLocale();
    }

    public boolean isArabicLocale() {
        if (getPreferenceAppLanguage() == null) {
            return getCurrentConfigLocale(mContext) == Language.ARABIC;
        } else {
            return getPreferenceAppLanguage() == Language.ARABIC;
        }
    }

    public enum Language {
        ARABIC,
        ENGLISH
    }

    public String getLocale() {
        if (isArabicLocale()) {
            return "ar";
        } else {
            return "en";
        }
    }

    private Language getCurrentConfigLocale(Context context) {

        String defaultLocale = context.getResources().getConfiguration().locale.getLanguage();

        if (defaultLocale.contains("ar")) {
            return Language.ARABIC;
        } else {
            return Language.ENGLISH;
        }
    }

    public void savePreferenceAppLanguage(String language) {
        PreferenceUtil util = new PreferenceUtil(mContext);
        util.setStringValue(APP_LANG_KEY, language);
    }

    private Language getPreferenceAppLanguage() {
        return getPreferenceLanguage(APP_LANG_KEY);
    }

    private Language getPreferenceLanguage(String key) {
        PreferenceUtil util = new PreferenceUtil(mContext);

        String currentLanguage = util.getStringValue(key, "");

        if (currentLanguage.equals(APP_LANG_KEY_AR)) {
            return Language.ARABIC;
        } else if (currentLanguage.equals(APP_LANG_KEY_EN)) {
            return Language.ENGLISH;
        } else {
//            return null;
            return Language.ARABIC; // enforce arabic
        }
    }

    public void initAppLocale() {

        // Set locale with Locale.setDefault will override system locale for this process (Locale.getDefault())
        // And there is not any documented way to retrieve the default locale after reset
        // So it is just enough to set locale for configuration and that will affect getResource method.

        Language language = getPreferenceAppLanguage();
        if (language == null) {
            return;
        }

        Locale locale = createLocale(language);

        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.locale = locale;

        mContext.getApplicationContext().getResources().updateConfiguration(config,
                mContext.getResources().getDisplayMetrics());
    }

    private Locale createLocale(Language language) {

        Locale locale;

        switch (language) {
            case ARABIC:
                locale = new Locale("ar", "SA");
                break;

            case ENGLISH:
            default:
                locale = new Locale("en", "US");
                break;
        }

        return locale;
    }
}
