package com.hajjassistant.model

import com.hajjassistant.items.MetaData


class Schedule {
    var title: String? = null
    var subtitle: String? = null
    var image: String? = null
    var metaData: MetaData? = null
}