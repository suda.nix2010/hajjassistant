package com.hajjassistant.app.di;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.hajjassistant.app.ApiEndpoint;
import com.hajjassistant.app.HajjApp;
import com.hajjassistant.app.Session;
import com.hajjassistant.domain.ApiErrorResponse;
import com.hajjassistant.utils.LocaleUtil;
import com.hajjassistant.utils.MobileValidator;
import com.hajjassistant.utils.PreferenceUtil;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import rx.Scheduler;


@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class, RxModule.class})
public interface AppComponent {

    void inject(HajjApp todoApp);

    HajjApp application();

    Context applicationContext();

    AssetManager assetManager();

    Gson gson();

    ApiEndpoint api();

    PreferenceUtil preferenceUtil();

    LocaleUtil localeUtil();

    Session session();

    Retrofit retrofit();

    Converter<ResponseBody, ApiErrorResponse> responseBodyConverter();

    MobileValidator mobileValidator();

    @UiThreadScheduler
    Scheduler uiThread();

    @IoThreadScheduler
    Scheduler ioThread();
}
