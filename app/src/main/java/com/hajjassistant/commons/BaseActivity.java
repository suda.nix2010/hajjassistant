package com.hajjassistant.commons;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.hajjassistant.app.HajjApp;
import com.hajjassistant.app.di.AppComponent;
import com.hajjassistant.utils.LocaleUtil;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = "BaseActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocaleUtil localeUtil = new LocaleUtil(this);
        localeUtil.initAppLocale();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected AppComponent getAppComponent() {
        return ((HajjApp) getApplication()).getAppComponent();
    }
}
