/*
 * Copyright (c) 2016. Aqar.sd
 */

package com.hajjassistant.commons;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.hajjassistant.app.HajjApp;
import com.hajjassistant.app.di.AppComponent;

public class BaseFragment extends Fragment {

    protected AppComponent getAppComponent() {
        return ((HajjApp) getActivity().getApplication()).getAppComponent();
    }

    protected void replaceFragment(Fragment fragment, int container) {
        replaceFragment(fragment, container, false);
    }

    protected void replaceFragmentWithBackStack(Fragment fragment, int container) {
        replaceFragment(fragment, container, true);
    }

    protected void replaceFragment(Fragment fragment, int container, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction()
                .replace(container, fragment, fragment.getTag());

        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.getTag());

        fragmentTransaction.commit();
    }

    protected void addFragment(Fragment fragment, int container, boolean addToBackStack) {
        FragmentManager fragmentManager = getFragmentManager();

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(container, fragment, fragment.getTag());

        if (addToBackStack)
            ft.addToBackStack(fragment.getTag());

        ft.commit();
    }

    protected void popup() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack();
    }

    protected void finishViewWithSuccess() {
        if (isAdded()) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }

}
