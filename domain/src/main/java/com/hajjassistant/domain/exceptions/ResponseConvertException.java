package com.hajjassistant.domain.exceptions;

public class ResponseConvertException extends Exception {

    public ResponseConvertException(String message) {
        super(message);
    }
}
