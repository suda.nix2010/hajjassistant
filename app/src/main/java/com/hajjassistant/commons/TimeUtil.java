package com.hajjassistant.commons;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeUtil {

    public static final String TAG = "TimeUtil";

    public static String getDate(String updatedAt) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        String date = null;

        try {
            Date d = dateFormat.parse(updatedAt);

            SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.US);
            date = dateFormatLocal.format(d);

            return date;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String getUpdatedAtPrettyTime(String updatedAt) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date = dateFormat.parse(updatedAt);
            SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.US);
            String format = dateFormatLocal.format(date);
            Date localDate = dateFormatLocal.parse(format);

            return new org.ocpsoft.prettytime.PrettyTime().format(localDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return updatedAt;
    }


    public static Date getCurrentGMTDate() {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.US);
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        String dateStr = dateFormatGmt.format(new Date());

        SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.US);
        try {
            Date gmtDate = dateFormatLocal.parse(dateStr);
            return gmtDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new Date();
    }
}
