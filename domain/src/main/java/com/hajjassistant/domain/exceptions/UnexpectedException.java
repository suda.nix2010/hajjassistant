package com.hajjassistant.domain.exceptions;

public class UnexpectedException extends Exception {

    public UnexpectedException(String message) {
        super(message);
    }
}
