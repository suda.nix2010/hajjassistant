package com.hajjassistant.domain.exceptions;

import com.hajjassistant.domain.ApiErrorResponse;

public class ApiException extends Exception {

    private ApiErrorResponse apiErrorResponse;

    public ApiException(ApiErrorResponse apiErrorResponse) {
        super("ApiException");
        this.apiErrorResponse = apiErrorResponse;
    }

    public ApiErrorResponse getApiErrorResponse() {
        return apiErrorResponse;
    }
}
