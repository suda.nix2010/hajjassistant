package com.hajjassistant.domain.exceptions;

public class UnauthorizeException extends Exception {

    public UnauthorizeException() {
        super("Unauthorize Exception please login again");
    }
}
