package com.hajjassistant.domain;

import com.google.gson.annotations.SerializedName;

public class ApiErrorResponse {

    public static int ERROR_CODE_LOGIN_SMS_LIMIT = 429;
    public static int ERROR_CODE_INVALID_MOBILE_NUMBER = 400;

    private String message;

    @SerializedName("errors")
    private ApiError error;

    private transient int httpCode;

    public ApiErrorResponse(String message, int httpCode) {
        this.message = message;
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public ApiError getError() {
        return error;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }
}
