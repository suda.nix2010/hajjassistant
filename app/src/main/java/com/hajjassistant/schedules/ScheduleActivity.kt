package com.hajjassistant.schedules

import android.os.Bundle
import android.view.MenuItem
import com.hajjassistant.R
import com.hajjassistant.commons.BaseActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_schedule.*
import kotlinx.android.synthetic.main.view_toolbar.*

class ScheduleActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)

        toolbar.title = "Schedules"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        Picasso.get().load("https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj02.jpg")
                .centerCrop()
                .fit()
                .into(imageView1)

        Picasso.get().load("https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj-1624x1185.jpg")
                .centerCrop()
                .fit()
                .into(imageView2)

        Picasso.get().load("https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj02.jpg")
                .centerCrop()
                .fit()
                .into(imageView3)

        Picasso.get().load("https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj-1624x1185.jpg")
                .centerCrop()
                .fit()
                .into(imageView4)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
