package com.hajjassistant.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.google.firebase.auth.FirebaseAuth
import com.hajjassistant.R
import com.hajjassistant.app.Session
import com.hajjassistant.chat.ChatFragment
import com.hajjassistant.chat.TTS
import com.hajjassistant.commons.BaseActivity
import com.hajjassistant.map.MapFragment
import com.hajjassistant.user.LoginActivity
import com.hajjassistant.utils.LocaleUtil
import com.hajjassistant.utils.PreferenceUtil
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_toolbar.*


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val REQUEST_AUDIO_PERMISSIONS_ID = 33

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        TTS.init(applicationContext)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText))
        setSupportActionBar(toolbar)


        supportFragmentManager.beginTransaction()
                .add(R.id.contentView, MainFragment())
                .commit()

        initDrawerLayout()

        initBottomNavigation()
        checkAudioRecordPermission()
    }

    private fun initBottomNavigation() {
        val item1 = AHBottomNavigationItem(
                R.string.feed, R.drawable.ic_rss_feed_white_24dp, R.color.colorAccent)
        val item2 = AHBottomNavigationItem(
                R.string.assistant, R.drawable.ic_assistant_white_24dp, R.color.colorAccent)
        val item3 = AHBottomNavigationItem(
                R.string.map, R.drawable.ic_map_white_24dp, R.color.colorAccent)

        bottomNavigation.addItem(item1)
        bottomNavigation.addItem(item2)
        bottomNavigation.addItem(item3)

        bottomNavigation.accentColor = Color.parseColor("#F63D2B")
        bottomNavigation.inactiveColor = Color.parseColor("#747474")

        bottomNavigation.setOnTabSelectedListener { position, wasSelected ->
            when (position) {
                0 -> pushFragment(MainFragment())
                1 -> pushFragment(ChatFragment())
                2 -> pushFragment(MapFragment())
            }

            true
        }
    }

    private fun pushFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(R.id.contentView, fragment, fragment.getTag())
                .commit()
    }

    private fun checkAudioRecordPermission() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.RECORD_AUDIO)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        REQUEST_AUDIO_PERMISSIONS_ID)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_AUDIO_PERMISSIONS_ID -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return
            }
        }
    }

    private fun initDrawerLayout() {
        val toggle = object : ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.open, R.string.close) {

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
            }
        }

        drawerLayout.setDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {

                FirebaseAuth.getInstance().signOut()

                val session = Session(PreferenceUtil(this), LocaleUtil(this))
                session.logout()
                Toast.makeText(this@MainActivity, getString(R.string.successfully_logged_out),
                        Toast.LENGTH_SHORT).show()

                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}