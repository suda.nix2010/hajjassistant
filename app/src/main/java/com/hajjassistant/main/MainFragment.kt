package com.hajjassistant.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.hajjassistant.HorizontalRecyclerAdapter
import com.hajjassistant.R
import com.hajjassistant.VerticalRecyclerAdapter
import com.hajjassistant.commons.BaseFragment
import com.hajjassistant.details.DetailsActivity
import com.hajjassistant.items.*
import com.hajjassistant.model.Schedule
import com.hajjassistant.model.Update
import com.hajjassistant.schedules.ScheduleActivity
import com.hajjassistant.updates.UpdateActivity
import kotlinx.android.synthetic.main.fragment_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainFragment : BaseFragment() {

    private val TAG = "MainFragment"

    private lateinit var adapter: VerticalRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchUpdates()

        recyclerView.setHasFixedSize(false)
        recyclerView.isNestedScrollingEnabled = true

        recyclerView.layoutManager = LinearLayoutManager(activity)

        adapter = VerticalRecyclerAdapter(ArrayList(), object : VerticalRecyclerAdapter.ItemClickListener {
            override fun onItemClicked(item: Item) {
                handleItemClick(item)
            }
        })

        recyclerView.adapter = adapter

        adapter.setOnItemClickListener(object : HorizontalRecyclerAdapter.OnListItemClickListener {
            override fun onListItemClick(view: View, position: Int) {
            }

            override fun onItemClicked(item: Item) {
                handleItemClick(item)
            }

            override fun onActionButtonClicked(item: Item) {
                if (item is Info2Item) {
                    val uri = String.format(Locale.ENGLISH, "geo:%f,%f", 21.406128, 39.873479)
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    activity?.startActivity(intent)
                } else if (item is InfoItem) {
                    startActivity(Intent(activity, ScheduleActivity::class.java))
                }
            }
        })
    }

    fun handleItemClick(item: Item) {
        when (item) {
            is MobileItem -> {
                val mobile = item.mobile
                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mobile, null))
                startActivity(intent)
            }
            is BannerItem -> {
                startActivity(Intent(activity, UpdateActivity::class.java))
            }
            is InfoItem -> {
                val intent = Intent(activity, DetailsActivity::class.java)
                intent.putExtra("title", item.title)
                intent.putExtra("subtitle", item.subtitle)
                intent.putExtra("image", item.image)
                startActivity(intent)
            }
            is MapItem -> {
                val uri = String.format(Locale.ENGLISH, "geo:%f,%f", item.latitude, item.longitude)
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                activity?.startActivity(intent)
            }

            is VideoItem -> {
                if (!TextUtils.isEmpty(item.externalLink)) {
                    val uri = Uri.parse(item.externalLink.replace("\u003d", "="))
                    val browserIntent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(browserIntent)
                }
            }
        }
    }

    private fun fetchUpdates() {
        val database = FirebaseDatabase.getInstance()
        val stationsRef = database.getReference("updates")

        stationsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val list = ArrayList<ArrayList<Item>>()

                for (stationSnapshot in dataSnapshot.children) {
                    val update = stationSnapshot.getValue(Update::class.java)

                    val bannerItem = ArrayList<Item>()
                    bannerItem.add(BannerItem(update?.title ?: "",
                            update?.subtitle ?: "",
                            update?.date ?: ""))

                    list.add(bannerItem)
                }

                progressBar.visibility = View.GONE
                adapter.addItemLists(list)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })

        fetchSchedules()
    }

    private fun fetchSchedules() {
        val database = FirebaseDatabase.getInstance()
        val stationsRef = database.getReference("schedules")

        stationsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val list = ArrayList<ArrayList<Item>>()
                val infoItem = ArrayList<Item>()

                for (stationSnapshot in dataSnapshot.children) {
                    val schedule = stationSnapshot.getValue(Schedule::class.java)

                    var metadate: MetaData? = null
                    if (schedule?.metaData != null) {
                        metadate = MetaData(schedule.metaData?.title ?: "",
                                schedule.metaData?.subtitle ?: "",
                                schedule.metaData?.actionButtonName ?: "",
                                schedule.metaData?.actionButtonColor ?: "#000")
                    }

                    infoItem.add(InfoItem(schedule?.title ?: "",
                            schedule?.image ?: "",
                            schedule?.subtitle ?: "",
                            metadate))
                }

                list.add(infoItem)
                adapter.addItemLists(list)

                dummyData()
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })
    }

    private fun dummyData() {
        val list = ArrayList<ArrayList<Item>>()

//        val bannerItem = ArrayList<Item>()
//        bannerItem.add(BannerItem("Live updates", "We will move to our tent in Mena before sunset. Make sure you are at the meeting point at 5:20 pm", "2 min ago"))
//        list.add(bannerItem)

//        val infoItem = ArrayList<Item>()
//        val metadate = MetaData("Today Schedule", "8 Du Al-Hijja", "View full schedule", "#4a90e2")
//        infoItem.add(InfoItem("Makkah", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj02.jpg", "Mena 2:40 pm", metadate))
//        infoItem.add(InfoItem("Mena", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj-1624x1185.jpg", "Mena 2:40 pm"))
//        infoItem.add(InfoItem("Makkah", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj02.jpg", "Mena 2:40 pm"))
//        infoItem.add(InfoItem("Mena", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj-1624x1185.jpg", "Mena 2:40 pm"))
//        list.add(infoItem)

        val info2Item = ArrayList<Item>()
        val metadate2 = MetaData("Where am I now?", "", "Take me to my group", "#f5a623")
        info2Item.add(Info2Item("Makkah", "https://designershaik.com/pub/media/boutique/store/image/static_map_963.png", "", metadate2))
        list.add(info2Item)

        val imageItems = ArrayList<Item>()
        imageItems.add(ImageItem("Makkah", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj02.jpg"))
        imageItems.add(ImageItem("Mena", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj-1624x1185.jpg"))
        list.add(imageItems)

        val imageItems2 = ArrayList<Item>()
        imageItems2.add(ImageItem("Quran", "https://www.thoughtco.com/thmb/_CKgL4-R3UmZkJMGwJBXFLBWkJE=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/10157735-56a5368c3df78cf77286f6f9.jpg"))
        imageItems2.add(ImageItem("Duaa", "http://muslimgirl.com/wp-content/uploads/2014/12/duaa-hands2-1024x681.jpg"))
        imageItems2.add(ImageItem("Arafa", "https://www.nmisr.com/wp-content/uploads/2017/08/%D9%8A%D9%88%D9%85-%D8%B9%D8%B1%D9%81%D8%A9-2.jpg"))
        list.add(imageItems2)

        adapter.addItemLists(list)
    }
}
