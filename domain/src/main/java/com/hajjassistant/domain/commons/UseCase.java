package com.hajjassistant.domain.commons;

import rx.Observable;

public interface UseCase<Q extends UseCase.RequestValues, T> {

    Observable<T> execute(Q requestValues);

    public interface RequestValues {

    }
}
