package com.hajjassistant.chat

import ai.api.AIListener
import ai.api.AIServiceException
import ai.api.RequestExtras
import ai.api.android.AIConfiguration
import ai.api.android.AIDataService
import ai.api.android.AIService
import ai.api.model.*
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.hajjassistant.HorizontalRecyclerAdapter
import com.hajjassistant.R
import com.hajjassistant.VerticalRecyclerAdapter
import com.hajjassistant.commons.BaseFragment
import com.hajjassistant.details.DetailsActivity
import com.hajjassistant.items.*
import com.hajjassistant.model.Schedule
import com.hajjassistant.schedules.ScheduleActivity
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.fragment_chat.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*


class ChatFragment : BaseFragment() {

    private var aiService: AIService? = null
    private var aiDataService: AIDataService? = null

    private lateinit var adapter: VerticalRecyclerAdapter

    val TAG = "ChatFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val config = AIConfiguration("f7e0e34f727949718fb3fd8dec3de126",
                ai.api.AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System)

        aiService = AIService.getService(activity, config)
        aiDataService = AIDataService(activity!!, config)

        aiService?.setListener(object : AIListener {
            override fun onResult(response: AIResponse?) {
                activity?.runOnUiThread({
                    progressBar.visibility = View.GONE
                    sendButton.visibility = View.VISIBLE
                    onAIResponse(response, true)
                })
            }

            override fun onListeningStarted() {
            }

            override fun onAudioLevel(level: Float) {
            }

            override fun onError(error: AIError?) {
                activity?.runOnUiThread({
                    progressBar.visibility = View.GONE
                    sendButton.visibility = View.VISIBLE
                    Log.d(TAG, "onError" + error.toString())
                })
            }

            override fun onListeningCanceled() {
                activity?.runOnUiThread({
                    progressBar.visibility = View.GONE
                    sendButton.visibility = View.VISIBLE
                    Log.d(TAG, "onCancelled")
                })
            }

            override fun onListeningFinished() {
                progressBar.visibility = View.GONE
                sendButton.visibility = View.VISIBLE
            }
        })

//        if (aiService is GoogleRecognitionServiceImpl) {
//            val svc = aiService as GoogleRecognitionServiceImpl
//            svc.setPartialResultsListener { partialResults ->
//                if (partialResults.isNotEmpty()) {
//                    val text = partialResults[0]
//                    if (!TextUtils.isEmpty(text)) {
//                        adapter.add(ChatSendItem(text))
//                    }
//                }
//            }
//        }

        recyclerView.setHasFixedSize(false)
        recyclerView.isNestedScrollingEnabled = true

        recyclerView.layoutManager = LinearLayoutManager(activity)
        adapter = VerticalRecyclerAdapter(ArrayList(), object : VerticalRecyclerAdapter.ItemClickListener {

            override fun onItemClicked(item: Item) {
                handleItemClick(item)
            }
        })
        recyclerView.adapter = adapter

        adapter.setOnItemClickListener(object : HorizontalRecyclerAdapter.OnListItemClickListener {
            override fun onListItemClick(view: View, position: Int) {
            }

            override fun onItemClicked(item: Item) {
                handleItemClick(item)
            }

            override fun onActionButtonClicked(item: Item) {
                if (item is Info2Item) {
                    val uri = String.format(Locale.ENGLISH, "geo:%f,%f", 21.406128, 39.873479)
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    activity?.startActivity(intent)
                } else if (item is InfoItem) {
                    startActivity(Intent(activity, ScheduleActivity::class.java))
                }
            }
        })

        RxTextView.textChanges(chatEditText).subscribe({
            if (TextUtils.isEmpty(chatEditText.text)) {
                sendButton.setImageResource(R.drawable.ic_mic_white_36dp)
            } else {
                sendButton.setImageResource(R.drawable.ic_send_white_24dp)
            }
        })

        sendButton.setOnClickListener {
            if (TextUtils.isEmpty(chatEditText.text)) {
                aiService?.startListening()
                progressBar.visibility = View.VISIBLE
                sendButton.visibility = View.GONE
            } else {
                val text = chatEditText.text.toString()

                if (!TextUtils.isEmpty(text)) {
                    adapter.add(ChatSendItem(text))
                    recyclerView.scrollToPosition(adapter.itemCount - 1)
                }

                chatEditText.text.clear()

                Observable.create(Observable.OnSubscribe<AIResponse> { observer ->
                    try {
                        Log.v(TAG, "Observable.create")

                        if (!observer.isUnsubscribed) {
                            val request = AIRequest()
                            val query = text

                            Log.v(TAG, "query $query")

                            if (!TextUtils.isEmpty(query))
                                request.setQuery(query)

                            val requestExtra = RequestExtras()
                            try {
                                val aiResponse = aiDataService?.request(request, requestExtra)
                                Log.v(TAG, "aiResponse $aiResponse")
                                observer.onNext(aiResponse)
                                observer.onCompleted()

                            } catch (e: AIServiceException) {
                                Log.v(TAG, "error ${e.response}")
                                val aiError = AIError(e)
                                observer.onError(Throwable(aiError.message))
                            }

                        }
                    } catch (e: Exception) {
                        Log.v(TAG, "error ${e.message}")
                        observer.onError(e)
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            onAIResponse(it)
                        }, {
                            Log.v(TAG, it?.message)
                        })
            }

        }

        adapter.add(ChatReceiveItem("How I can help you?"))
    }

    override fun onPause() {
        super.onPause()

        if (aiService != null) {
            aiService?.pause()
        }
    }

    override fun onResume() {
        super.onResume()

        if (aiService != null) {
            aiService?.resume()
        }
    }

    private fun onAIResponse(response: AIResponse?, showQuery: Boolean = false) {
        Log.d(TAG, "onResult")
        val jsonRes = Gson().toJson(response)
        Log.d(TAG, "onResult$jsonRes")

        Log.i(TAG, "Received success response")

        val status = response?.status
        Log.i(TAG, "Status code: " + status?.code)
        Log.i(TAG, "Status type: " + status?.errorType)

        val result = response?.result
        Log.i(TAG, "Resolved query: " + result?.resolvedQuery)

        if (showQuery && !TextUtils.isEmpty(result?.resolvedQuery)) {
            adapter.add(ChatSendItem(result?.resolvedQuery ?: ""))
            recyclerView.scrollToPosition(adapter.itemCount - 1)
        }

        Log.i(TAG, "Action: " + result?.action)


        val fulfillment = result?.fulfillment

        val speech = fulfillment?.speech
        Log.i(TAG, "Speech: $speech")

        TTS.speak(speech)

        val imageItems = ArrayList<Item>()

        val messages = fulfillment?.messages
        if (messages != null && messages.isNotEmpty()) {
            for (message in messages) {
                when (message) {
                    is ResponseMessage.ResponseSpeech -> {
                        adapter.add(ChatReceiveItem(speech ?: ""))
                        recyclerView.scrollToPosition(adapter.itemCount - 1)
                    }
                    is ResponseMessage.ResponseCard -> {
                        val title = message.title
                        val subtitle = message.subtitle
                        val imageUrl = message.imageUrl
                        imageItems.add(ImageItem(title, imageUrl))
                    }
                    is ResponseMessage.ResponseImage -> {
                        val imageUrl = message.imageUrl
                        imageItems.add(ImageItem("", imageUrl))
                    }
                    is ResponseMessage.ResponsePayload -> {
                        val payload = message.payload
                        if (payload.has("resources")) {
                            val jsonArray = payload.getAsJsonArray("resources")
                            for (jsonElement in jsonArray) {
                                val jsonObject = jsonElement.asJsonObject
                                val url = jsonObject.get("resourceUrl").asString
                                val value = jsonObject.get("resourceValue").asString
                                val title = jsonObject.get("title").asString
                                val type = jsonObject.get("resourceType").asInt
                                val thumbnail = if (jsonObject.has("thumbnail")) jsonObject.get("thumbnail").asString else ""
                                val externalLink = if (jsonObject.has("externalLink")) jsonObject.get("externalLink").asString else ""

                                when (type) {
                                    1 -> {
                                        Log.v(TAG, "Added $url")
                                        imageItems.add(ImageItem(title ?: "", url.toString()))
                                    }
                                    2 -> {
                                        Log.v(TAG, "Value $value")
                                        imageItems.add(MapItem(title, "https://maps.googleapis.com/maps/api/staticmap?center=مكة&zoom=11&scale=1&size=600x300&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7Cمكة", value.split(',')[0].toDouble(), value.split(',')[1].toDouble()))
                                    }
                                    3 -> {
                                        imageItems.add(MobileItem(value))
                                    }
                                    4 -> {
                                        imageItems.add(VideoItem(value, thumbnail, externalLink))
                                    }
                                    5 -> {
                                        fetchSchedules()
                                    }
                                }

                                Log.v(TAG, "Payload $url, $value, $type")
                            }
                        }
                    }

                    is GoogleAssistantResponseMessages.ResponseBasicCard -> {
                        val title = message.title
                        val subtitle = message.subtitle
                        val imageUrl = message.image.url
                        imageItems.add(ImageItem(title, imageUrl))
                    }
                }
            }

            if (imageItems.isNotEmpty()) {
                adapter.addList(imageItems)
                recyclerView.scrollToPosition(adapter.itemCount - 1)
            }
        }

        val metadata = result?.metadata
        if (metadata != null) {
            Log.i(TAG, "Intent id: " + metadata.intentId)
            Log.i(TAG, "Intent name: " + metadata.intentName)
        }

        val params = result?.parameters
        if (params != null && !params.isEmpty()) {
            Log.i(TAG, "Parameters: ")
            val entries = params.entries
            for (entry in entries) {
                Log.i(TAG, String.format("%s: %s", entry.key, entry.value.toString()))
            }
        }
    }

    private fun dummyData() {
        val list = ArrayList<ArrayList<Item>>()

        for (i in 0..49) {
            val chat = ArrayList<Item>()
            if (i % 2 == 0) {
                chat.add(ChatSendItem("Hello"))
            } else {
                chat.add(ChatReceiveItem("Hi"))
            }

            list.add(chat)
        }

        val imageItems = ArrayList<Item>()
        imageItems.add(ImageItem("Makkah", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj02.jpg"))
        imageItems.add(ImageItem("Mena", "https://cdn.forbesmiddleeast.com/en/wp-content/uploads/sites/3/2016/10/Hajj-1624x1185.jpg"))
        list.add(imageItems)
    }

    fun handleItemClick(item: Item) {
        when (item) {
            is MobileItem -> {
                val mobile = item.mobile
                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mobile, null))
                startActivity(intent)
            }
            is BannerItem -> {

            }
            is InfoItem -> {
                val intent = Intent(activity, DetailsActivity::class.java)
                intent.putExtra("title", item.title)
                intent.putExtra("subtitle", item.subtitle)
                intent.putExtra("image", item.image)
                startActivity(intent)
            }
            is MapItem -> {
                val uri = String.format(Locale.ENGLISH, "geo:%f,%f", item.latitude, item.longitude)
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                activity?.startActivity(intent)
            }

            is VideoItem -> {
                if (!TextUtils.isEmpty(item.externalLink)) {
                    val uri = Uri.parse(item.externalLink.replace("\u003d", "="))
                    val browserIntent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(browserIntent)
                }
            }
        }
    }

    private fun fetchSchedules() {
        val database = FirebaseDatabase.getInstance()
        val stationsRef = database.getReference("schedules")

        stationsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val list = ArrayList<ArrayList<Item>>()
                val infoItem = ArrayList<Item>()

                for (stationSnapshot in dataSnapshot.children) {
                    val schedule = stationSnapshot.getValue(Schedule::class.java)

                    var metadate: MetaData? = null
                    if (schedule?.metaData != null) {
                        metadate = MetaData(schedule.metaData?.title ?: "",
                                schedule.metaData?.subtitle ?: "",
                                schedule.metaData?.actionButtonName ?: "",
                                schedule.metaData?.actionButtonColor ?: "#000")
                    }

                    infoItem.add(InfoItem(schedule?.title ?: "",
                            schedule?.image ?: "",
                            schedule?.subtitle ?: "",
                            metadate))
                }

                list.add(infoItem)
                adapter.addItemLists(list)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })
    }

}
