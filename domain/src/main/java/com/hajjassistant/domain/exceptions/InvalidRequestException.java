package com.hajjassistant.domain.exceptions;

import java.util.List;

public class InvalidRequestException extends Exception {

    private List<Integer> mErrors;

    public InvalidRequestException(List<Integer> errors) {
        super("InvalidRequestException");
        mErrors = errors;
    }

    public List<Integer> getErrors() {
        return mErrors;
    }
}
