package com.hajjassistant.app.di;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module
public class RxModule {

    @Provides
    @UiThreadScheduler
    public Scheduler provideObserveOnScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @IoThreadScheduler
    public Scheduler provideSubscribeOnScheduler() {
        return Schedulers.io();
    }
}
