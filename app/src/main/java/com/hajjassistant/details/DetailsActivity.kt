package com.hajjassistant.details

import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import com.hajjassistant.R
import com.hajjassistant.commons.BaseActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.view_toolbar.*

class DetailsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val title = intent.getStringExtra("title")
        val desc = intent.getStringExtra("subtitle")
        val imageUrl = intent.getStringExtra("image")

        toolbar.title = title ?: "Details"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.get()
                    .load(imageUrl)
                    .centerCrop()
                    .fit()
                    .into(imageView)
        }

        if (!TextUtils.isEmpty(title)) {
            titleTextView.text = title
        }

        if (!TextUtils.isEmpty(desc)) {
            descTextView.text = desc
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
