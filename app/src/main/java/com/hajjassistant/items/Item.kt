package com.hajjassistant.items

open class Item(val title: String)

class MobileItem(val mobile: String) : Item("")

open class ImageItem(title: String, val image: String) : Item(title)
class VideoItem(title: String, val image: String, val externalLink: String) : Item(title)

class ChatSendItem(text: String) : Item(text)
class ChatReceiveItem(text: String) : Item(text)

class MapItem(title: String,
              image: String,
              val latitude: Double?,
              val longitude: Double?) : ImageItem(title, image)

open class InfoItem(title: String,
               image: String,
               val subtitle: String?,
               val metaData: MetaData? = null) : ImageItem(title, image)

class Info2Item(title: String,
               image: String,
               subtitle: String?,
               metaData: MetaData? = null) : InfoItem(title, image, subtitle, metaData)

class ExternalLinkItem(title: String,
                       image: String,
                       val link: String?) : ImageItem(title, image)

class BannerItem(title: String, val desc: String, val date: String) : Item(title)

class MetaData() {
    var title: String? = null
    var subtitle: String? = null
    var actionButtonName: String? = null
    var actionButtonColor: String? = null

    constructor(title:String?, subtitle: String?, actionButtonName: String?,
                actionButtonColor: String?) : this() {
        this.title = title
        this.subtitle = subtitle
        this.actionButtonName = actionButtonName
        this.actionButtonColor = actionButtonColor
    }
}

