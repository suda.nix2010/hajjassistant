package com.hajjassistant.commons.widgets;

import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hajjassistant.R;

public class StatefulGroupView extends RelativeLayout {
    public static final String TAG = StatefulGroupView.class.getSimpleName();
    private View mRootView;
    private Context mContext;

    private View mProgressView;
    private View mContentView;
    private View mConnectionErrorView;
    private FrameLayout mEmptyView;
    private View mBusinessErrorView;

    private Button mTryAgainButton;
    private TextView mBusinessErrorTextView;

    private TryAgainClickListener mTryAgainClickListener;
    private EmptyContentCallback mIsEmptyContentCallback;


    public interface TryAgainClickListener {
        void onTryAgainClicked();
    }

    public interface EmptyContentCallback {
        boolean isEmptyContent();
    }

    public enum State {
        PROGRESS_STATE,
        NORMAL_STATE,
        NO_ITEM_FOUND_STATE,
        TIMEOUT_STATE,
        BUSINESS_ERROR_STATE
    }

    public StatefulGroupView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRootView = inflater.inflate(getLayoutId(), this, true);

        if (isInEditMode()) {
            return;
        }

        mContext = context;

        setGravity(Gravity.CENTER_VERTICAL);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        this.setLayoutParams(lp);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initMember();
    }

    private void initMember() {
        mProgressView = mRootView.findViewById(R.id.progressView);
        mConnectionErrorView = mRootView.findViewById(R.id.connectionErrorView);
        mEmptyView = (FrameLayout) mRootView.findViewById(R.id.emptyView);
        mContentView = mRootView.findViewById(R.id.contentView);
        mBusinessErrorView = mRootView.findViewById(R.id.businessErrorView);

        if (mContentView == null) {
            throw new RuntimeException(TAG + ":must have child view with id contentView");
        }

        mTryAgainButton = (Button) mRootView.findViewById(R.id.tryAgainButton);
        mBusinessErrorTextView = (TextView) mRootView.findViewById(R.id.businessErrorTextView);
    }

    public void init(TryAgainClickListener tryAgainClickListener, EmptyContentCallback emptyContentCallback) {
        mTryAgainClickListener = tryAgainClickListener;
        mIsEmptyContentCallback = emptyContentCallback;

        mTryAgainButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTryAgainClickListener != null) {
                    mTryAgainClickListener.onTryAgainClicked();
                }
            }
        });
    }

    public void goToState(State state) {
        resetGroupView();

        switch (state) {
            case NORMAL_STATE:
                mContentView.setVisibility(View.VISIBLE);
                break;
            case PROGRESS_STATE:
                mProgressView.setVisibility(View.VISIBLE);
                break;
            case NO_ITEM_FOUND_STATE:
                mEmptyView.setVisibility(View.VISIBLE);
                break;
            case TIMEOUT_STATE:
                mConnectionErrorView.setVisibility(View.VISIBLE);
                break;
            case BUSINESS_ERROR_STATE:
                mBusinessErrorView.setVisibility(View.VISIBLE);
                break;
            default:
                mContentView.setVisibility(View.VISIBLE);
        }
    }

    public void updateState() {
        if (mIsEmptyContentCallback.isEmptyContent()) {
            goToState(State.NO_ITEM_FOUND_STATE);

        } else {
            goToState(State.NORMAL_STATE);
        }
    }

    protected int getLayoutId() {
        return R.layout.view_stateful_groupview;
    }

    private void resetGroupView() {
        mProgressView.setVisibility(View.GONE);
        mContentView.setVisibility(View.GONE);
        mConnectionErrorView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.GONE);
        mBusinessErrorView.setVisibility(View.GONE);
    }

    public View getContentView() {
        return mContentView;
    }

    public View getProgressView() {
        return mProgressView;
    }

    public View getConnectionErrorView() {
        return mConnectionErrorView;
    }

    public FrameLayout getEmptyView() {
        return mEmptyView;
    }

    public void setEmptyView(View view) {
        mEmptyView.removeAllViews();
        mEmptyView.addView(view);
    }

    public void setBusinessError(@StringRes int resId) {
        if (mBusinessErrorTextView != null) {
            mBusinessErrorTextView.setText(resId);
        }
    }

    public void setBusinessError(String error) {
        if (mBusinessErrorTextView != null) {
            mBusinessErrorTextView.setText(error);
        }
    }

}
