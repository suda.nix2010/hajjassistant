package com.hajjassistant

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.hajjassistant.items.*
import com.squareup.picasso.Picasso
import java.util.*

class HorizontalRecyclerAdapter(private val items: ArrayList<Item>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var listItemClickListener: OnListItemClickListener? = null

    override fun getItemViewType(position: Int): Int {
        val item = items?.get(position)
        return when (item) {
            is ChatSendItem -> VerticalRecyclerAdapter.ItemTypes.CHAT_SEND_ITEM.value
            is ChatReceiveItem -> VerticalRecyclerAdapter.ItemTypes.CHAT_RECEIVE_ITEM.value
            is MapItem -> VerticalRecyclerAdapter.ItemTypes.MAP_ITEM.value
            is Info2Item -> VerticalRecyclerAdapter.ItemTypes.INFO2_ITEM.value
            is InfoItem -> VerticalRecyclerAdapter.ItemTypes.INFO_ITEM.value
            is ExternalLinkItem -> VerticalRecyclerAdapter.ItemTypes.EXTERNAL_LINK_ITEM.value
            is MobileItem -> VerticalRecyclerAdapter.ItemTypes.MOBILE_ITEM.value
            is VideoItem -> VerticalRecyclerAdapter.ItemTypes.VIDEO_ITEM.value
            else -> VerticalRecyclerAdapter.ItemTypes.ITEM.value
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)

        return when (viewType) {
            VerticalRecyclerAdapter.ItemTypes.INFO2_ITEM.value -> {
                InfoItemViewHolder(inflater.inflate(R.layout.info2_item_view, viewGroup, false))
            }
            VerticalRecyclerAdapter.ItemTypes.INFO_ITEM.value -> {
                InfoItemViewHolder(inflater.inflate(R.layout.info_item_view, viewGroup, false))
            }
            VerticalRecyclerAdapter.ItemTypes.MOBILE_ITEM.value -> {
                VerticalRecyclerAdapter.MobileItemViewHolder(inflater.inflate(R.layout.mobile_item_view, viewGroup, false))
            }
            VerticalRecyclerAdapter.ItemTypes.VIDEO_ITEM.value -> {
                VerticalRecyclerAdapter.VideoItemViewHolder(inflater.inflate(R.layout.video_item_view, viewGroup, false))
            }
            else -> {
                MapItemViewHolder(inflater.inflate(R.layout.image_item_view, viewGroup, false))
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        val item = items?.get(position)

        when (viewHolder) {
            is MapItemViewHolder -> {
                if (item is ImageItem) {
                    viewHolder.textView.text = item.title
                    if (!TextUtils.isEmpty(item.image)) {
                        Picasso.get()
                                .load(item.image)
                                .centerCrop()
                                .fit()
                                .into(viewHolder.imageView)
                    }

                    viewHolder.rootView.setOnClickListener {
                        listItemClickListener?.onItemClicked(item)
                    }
                }
            }

            is Info2ItemViewHolder -> {
                if (item is Info2Item) {
                    viewHolder.title.text = item.title
                    viewHolder.subtitle.text = item.subtitle
                    if (!TextUtils.isEmpty(item.image)) {
                        Picasso.get()
                                .load(item.image)
                                .centerCrop()
                                .fit()
                                .into(viewHolder.imageView)
                    }

                    viewHolder.rootView.setOnClickListener {
                        listItemClickListener?.onItemClicked(item)
                    }
                }
            }

            is InfoItemViewHolder -> {
                if (item is InfoItem) {
                    viewHolder.title.text = item.title
                    viewHolder.subtitle.text = item.subtitle
                    if (!TextUtils.isEmpty(item.image)) {
                        Picasso.get()
                                .load(item.image)
                                .centerCrop()
                                .fit()
                                .into(viewHolder.imageView)
                    }

                    viewHolder.rootView.setOnClickListener {
                        listItemClickListener?.onItemClicked(item)
                    }
                }
            }

            is VerticalRecyclerAdapter.MobileItemViewHolder -> {
                if (item is MobileItem) {
                    val mobile = item.mobile

                    viewHolder.rootView.setOnClickListener {
                        listItemClickListener?.onItemClicked(item)
                    }
                }
            }

            is VerticalRecyclerAdapter.VideoItemViewHolder -> {
                if (item is VideoItem) {
                    viewHolder.textView.text = item.title
                    if (!TextUtils.isEmpty(item.image)) {
                        Picasso.get()
                                .load(item.image)
                                .centerCrop()
                                .fit()
                                .into(viewHolder.imageView)
                    }

                    viewHolder.rootView.setOnClickListener {
                        listItemClickListener?.onItemClicked(item)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    interface OnListItemClickListener {
        fun onListItemClick(view: View, position: Int)
        fun onItemClicked(item: Item)
        fun onActionButtonClicked(item: Item)
    }

    fun setOnItemClickListener(listItemClickListener: OnListItemClickListener?) {
        this.listItemClickListener = listItemClickListener
    }

    private inner class MapItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView = itemView
        val textView: TextView = itemView.findViewById<View>(R.id.text) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.imageView) as ImageView
    }

    private inner class InfoItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView = itemView
        val title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        val subtitle: TextView = itemView.findViewById<View>(R.id.subtitle) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.imageView) as ImageView
    }

    private inner class Info2ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView = itemView
        val title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        val subtitle: TextView = itemView.findViewById<View>(R.id.subtitle) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.imageView) as ImageView
    }

}