package com.hajjassistant.domain;

import com.google.gson.annotations.SerializedName;

public class ApiError {

    private String param;

    @SerializedName("msg")
    private String message;

    public ApiError() {
    }

    public String getParam() {
        return param;
    }

    public String getMessage() {
        return message;
    }
}
