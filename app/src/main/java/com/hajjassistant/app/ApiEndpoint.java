package com.hajjassistant.app;

import android.content.Context;

import com.hajjassistant.R;

public class ApiEndpoint {

    private String mBaseUrl;
    private String mApiResources;

    public ApiEndpoint(Context context) {
        mBaseUrl = context.getResources().getString(R.string.BASE_URL);
        mApiResources = context.getResources().getString(R.string.API_RESOURCES);
    }

    public ApiEndpoint(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return mBaseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    public String getApiUrl() {
        return mBaseUrl + mApiResources;
    }

    public String getURL(String url, String resourcePath) {
        return url + resourcePath;
    }

    public String getURL(String resourcePath) {
        return getApiUrl() + resourcePath;
    }
}
