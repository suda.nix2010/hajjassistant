/*
 * Copyright (c) 2017. Aqar.sd
 */

package com.hajjassistant.commons;

public class Constant {

    public static final String DEEP_LINK_URL_PREFIX = "properties/";
    public static final String DEEP_LINK_PROPERTY_ID = "property_id";
    public static final String DEEP_LINK_USER_URL_PREFIX = "users/";
    public static final String DEEP_LINK_USER_ID = "user_id";

    public static final String SUDAN_ADDRESS_NAME = "sudan";

    public static final Double SUDAN_CENTER_LAT = 15.5888852;
    public static final Double SUDAN_CENTER_LNG = 32.5243864;
    public static final float SUDAN_CENTER_ZOOM_LEVEL = 11.6f;

    public static final String LOCAL_DATE_FORMAT = "yyyy-MMM-dd HH:mm:ss";
}
